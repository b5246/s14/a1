console.log("Hello, world!");

let firstname;
let lastname;
let age;
let hobby = ["Biking",
	"Mountain Climbing",
	"Swimming"]
let address = {
	houseNumber: "32",
	street: "Washington",
	city: "Lincoln",
	state: "Nebraska"
}
let isMarried;

function printUserInfo(firstname,lastname,age,hobby,address){
	console.log("First Name: " + firstname);
	console.log("Last Name: " + lastname);
	console.log("Age: " + age);
	console.log("Hobbies:");
	console.log(hobby);
	console.log("Work Address:");
	console.log(address);
}
printUserInfo("John","Smith","30",hobby,address);

function returnFunction(firstname,lastname,age,hobby,address){
	console.log(firstname+" "+lastname+" is "+age+" years of age.");
	console.log("This was printed inside of the function");
	console.log(hobby);
	console.log("This was printed inside of the function");
	console.log(address);
	return isMarried = true;
}

// console.log(returnFunction("John","Smith","30",hobby,address));
console.log("The value of isMarried is:" + returnFunction("John","Smith","30",hobby,address));